﻿namespace Challenge3
{
    class Program
    {
        async static Task Main(string[] args)
        {
            await PrintAsync();
            Thread.Sleep(500);
            Console.WriteLine("Конец метода Main");
        }

        static void Print()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"Выполнена операция #{i}");
                Thread.Sleep(200);
            }
        }

        async static Task PrintAsync()
        {
            Console.WriteLine("Начало метода PrintAsync");
            await Task.Run(() => Print());
            Console.WriteLine("Конец метода PrintAsync");
        }
    }
}