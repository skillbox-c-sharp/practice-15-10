﻿namespace Challenge2
{
    internal class Program
    {
        static object o = new object();
        static CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        static void Method(object text)
        {
            string inputString = (string)text;
            lock (o)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (cancellationTokenSource.Token.IsCancellationRequested)
                    {
                        Console.WriteLine($"Запрос на отмену задачи с текстом {inputString}");
                        try
                        {
                            cancellationTokenSource.Token.ThrowIfCancellationRequested();
                        }
                        catch (OperationCanceledException ex)
                        {
                            Console.WriteLine($"Отмена задачи {inputString} {ex.Message}");
                            return;
                        }
                    }

                    Console.WriteLine($"Выполнение задачи с текстом {inputString}");
                    Thread.Sleep(300);
                }
            }
        }

        static void Main(string[] args)
        {
            Task t1 = Task.Factory.StartNew(Method, "Текст 1-го таска", cancellationTokenSource.Token);
            Task t2 = Task.Factory.StartNew(Method, "Текст 2-го таска", cancellationTokenSource.Token);

            Thread.Sleep(4000);

            cancellationTokenSource.Cancel();

            CancellationTokenSource cnlSrc = new CancellationTokenSource();

            Task t3 = Task.Factory.StartNew(() => {
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine("Выполнение задачи с текстом Текст 3-го таска");
                    Thread.Sleep(300);
                }
                });

            t3.Wait();

            Console.ReadLine();
        }
    }
}