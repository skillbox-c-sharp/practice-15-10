﻿namespace Challenge1
{
    internal class Program
    {
        static void Method(object msgType)
        {
            int msgTypeInt = (int)msgType;

            switch (msgTypeInt)
            {
                case 0:
                    Console.WriteLine($"Нулевой тип сообщения в потоке {Thread.CurrentThread.Name}");
                    Thread.Sleep(1000);
                    break;
                case 1:
                    Console.WriteLine($"Первый тип сообщения в потоке {Thread.CurrentThread.Name}");
                    Thread.Sleep(2000);
                    break;
                default:
                    Console.WriteLine($"Дефолтный тип сообщения в потоке {Thread.CurrentThread.Name}");
                    Thread.Sleep(500);
                    break;        
            }
        }

        static void Main(string[] args)
        {
            Thread t1 = new Thread(Method);
            t1.Name = "FirstThread";
            t1.Start(0);
            t1.Join();

            Thread t2 = new Thread(Method);
            t2.Name = "SecondThread";
            t2.Start(0);

            Thread t3 = new Thread(Method);
            t3.Name = "SecondThread";
            t3.Start(2);

            Console.ReadKey();
        }
    }
}